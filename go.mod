module chainmaker.org/chainmaker/store-tikv/v2

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.2
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/procfs v0.6.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/yiyanwannian/client-go v1.0.2
)

replace google.golang.org/grpc v1.40.0 => google.golang.org/grpc v1.26.0
